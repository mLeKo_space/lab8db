﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;

namespace SQLServer
{
    class Program
    {
        static void SELECTQuery(SqlConnection connection, string filePath, string tableName) //Запит SELECT
        {
            using (SqlCommand sqlCommand = new SqlCommand($"select * from {tableName};", connection)) //Формування запиту
            {
                SqlDataReader returner = sqlCommand.ExecuteReader(); //Створення об'єкта зчитування повернутих даних
                string returned_result = ""; //Змінна повернутих даних
                for (int i = 0; i < returner.FieldCount; i++) returned_result += $"{returner.GetName(i),-63}"; //Формування заголовку таблиці
                returned_result += "\n"; //Перехід на наступний рядок
                while (returner.Read()) //Цикл зчитування
                {
                    for (int i = 0; i < returner.FieldCount; i++) returned_result += $"{returner.GetValue(i),-63}"; //Формування рядка таблиці з об'єкту зчитування
                    returned_result += "\n"; //Перехід на наступний рядок
                }
                File.WriteAllText(filePath, returned_result); //Вивід сформованої таблиці у файл
                returner.Close();
            }
        }

        static void SELECTSpecificQuery(SqlConnection connection, string filePath, string tableName, string columnName) //Запит SELECT для певних стовпців
        {
            using (SqlCommand sqlCommand = new SqlCommand($"select {columnName} from {tableName};", connection)) //Формування запиту
            {
                SqlDataReader returner = sqlCommand.ExecuteReader(); //Створення об'єкта зчитування повернутих даних
                string returned_result = ""; //Змінна повернутих даних

                for (int i = 0; i < returner.FieldCount; i++) returned_result += $"{returner.GetName(i),-63}"; //Формування заголовку таблиці
                returned_result += "\n"; //Перехід на наступний рядок
                while (returner.Read()) //Цикл зчитування
                {
                    for (int i = 0; i < returner.FieldCount; i++) returned_result += $"{returner.GetValue(i),-63}"; //Формування рядка таблиці з об'єкту зчитування
                    returned_result += "\n"; //Перехід на наступний рядок
                }
                File.WriteAllText(filePath, returned_result); //Вивід сформованої таблиці у файл
                returner.Close();
            }
        }
        static void INSERTQuery(SqlConnection connection, string table) //Запит INSERT
        {
            string[] fields; //Масив рядків стовпців таблиці
            string fields_str = "", values = ""; //Змінні запиту
            int values_number; //Змінна зберігання кількості рядків для вставки
            using (SqlCommand sqlCommand = new SqlCommand($"select top (0) * from {table}", connection)) //Запит отримання назв стовпців
            {
                SqlDataReader reader = sqlCommand.ExecuteReader(); //Об'єкт зчитування даних
                fields = new string[reader.FieldCount]; //Ініціалізація масиву назв стовпців
                for (int i = 0; i < fields.Length; i++) //Цикл проходження стовпцями таблиці
                {
                    fields[i] = reader.GetName(i); //Запис назви стовпця в змінну масиву
                    fields_str += fields[i] + (i != fields.Length - 1 ? "," : ""); //Запис назви стовпця в шаблон для запиту
                }
                reader.Close();
            }
            while (true) //Цикл обробки виключень
            {
                Console.Write("Введіть кількість рядків для вставлення в таблицю: ");
                if (int.TryParse(Console.ReadLine(), out values_number) == false || values_number <= 0) //Зчитування та обробка кількості рядків для запиту
                {
                    Console.WriteLine(inputError); //Вивід помилки введення
                    continue; //Перехід на наступну ітерацію циклу
                }
                break; //Вихід
            }
            Console.WriteLine("Введіть значення для записів:");
            for (int i = 0; i < values_number; i++) //Цикл введення даних запиту
            {
                Console.WriteLine($"{i + 1}-й запис:"); //Виведення номеру запиту
                values += "("; //Додавання у шаблон запиту відкритої дужки
                for (int j = 0; j < fields.Length; j++) //Цикл проходження назвами стовпців
                {
                    Console.Write($"{fields[j]}: "); //Виведення назви стовпця
                    values += Console.ReadLine() + (j != (fields.Length - 1) ? "," : ""); //Розширення шаблона запиту прочитаним значенням та комою (якщо стовпець не останній)
                }
                values += ")" + (i != values_number - 1 ? "," : ""); //Закриття дужки в шаблоні та кома (якщо запис не останній)
            }
            using (SqlCommand command = new SqlCommand($"INSERT INTO {table} ({fields_str}) VALUES {values};", connection)) //Формування команди запиту
            Console.WriteLine($"Додано об'єктів: {command.ExecuteNonQuery()}"); //Виконання команди та повернення кількість доданих елементів


        }
        static void DELETEQuery(SqlConnection connection, string table) //Запиту DELETE
        {
            Console.Write("Введіть умову видалення записів з таблиці: ");
            string delete = Console.ReadLine(); //Зчитування умови для видалення
            using (SqlCommand command = new SqlCommand($"DELETE FROM {table} WHERE {delete};", connection)) //Формування запиту
            Console.WriteLine($"Видалено об'єктів: {command.ExecuteNonQuery()}"); //Виконання команди та повернення кількості видалених елементів
        }
        static void UPDATEQuery(SqlConnection connection, string table) //Запит UPDATE
        {
            Console.Write("Введіть умову оновлення записів: "); 
            string update_condition = Console.ReadLine(); //Зчитування умови зміни записів
            Console.Write("Введіть вираз оновлення стовпців: ");
            string update_change = Console.ReadLine(); //Зчиитування змінюваного виразу
            using (SqlCommand command = new SqlCommand($"UPDATE {table} SET {update_change} WHERE {update_condition};", connection)) //Формування запиту
            Console.WriteLine($"Оновлено об'єктів: {command.ExecuteNonQuery()}"); //Виконання команди та повернення кількості змінених елементів
        }

        static void Menu()
        {
            string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\mLeKo\Desktop\lab8\library.mdf;Integrated Security=True"; //Стрічка з параметрами з'єднання з сервером
            using (SqlConnection connection = new SqlConnection(connectionString)) //З'єднання з сервером
            {
                connection.Open(); //Відкриття з'єднання

                //Цикл реалізації меню
                while (true)
                {
                    Console.WriteLine("Меню:\n0 - вихід, \n1 - прочитати дані окремих стовпців,\n2 - прочитати дані всієї таблиці,\n3 - додати записи до таблиці," +
                        "\n4 - видалити записи з таблиці,\n5 - оновити дані таблиці.");
                    uint menu, table; //Змінні навігації по меню та вибору таблиці
                    Console.Write("SQLClient> ");
                    if (uint.TryParse(Console.ReadLine(), out menu) == false || menu > 5) //Обробка помилок
                    {
                        Console.WriteLine(inputError); //Повідомлення про помилку
                        continue; //Перейти на наступну ітерацію
                    }
                    if (menu == 0) { Console.WriteLine("Вихід."); break; } //Вихід
                    if (menu == 1) //
                    {
                        Console.WriteLine("\nТаблиці:\n0 - вихід,\n1 - adult,\n2 - copy,\n3 - item,\n4 - juvenile,\n5 - loan," +
                            "\n6 - loanhist,\n7 - member,\n8 - reservation,\n9 - title.");
                        Console.Write("SQLClient > ");;
                        if (uint.TryParse(Console.ReadLine(), out table) == false || table > 9) Console.WriteLine(inputError); //Перевірка правильності номера таблиці

                        string columnName;
                        if (table == 0) { Console.WriteLine("Вихід."); break; }
                        if (table == 1)
                        {
                            Console.WriteLine("\nСтовпці:\n1 - member_no,\n2 - street,\n3 - city,\n4 - state,\n5 - zip,\n6 - phone_no,\n7 - expr_date " +
                                "\n\n!!!Введіть назву стовпця. Якщо стовпців декілька - вводити через кому!!!");
                            Console.Write("\nSQLClient > ");
                            columnName = Console.ReadLine();
                            if (columnName == null) Console.WriteLine(inputError);
                            else
                            {
                                Console.Write("Шлях до файлу: ");
                                string file_path = Console.ReadLine(); //Зчитування шляху файла, в який буде записана вибірка
                                if (columnName != null) //Якщо вибрано не вихід з меню запиту вибірки
                                    Console.WriteLine("Запит виконується...");
                                    try { SELECTSpecificQuery(connection, file_path, tables[table], columnName); } //Обробка виключень та виклик SELECT-запиту
                                    catch (Exception exception) { Console.WriteLine(exception.Message); } //Виведення повідомлення помилки
                            }
                        }
                        if (table == 2)
                        {
                            Console.WriteLine("\nСтовпці:\n1 - isbn,\n2 - copy_no,\n3 - title_no,\n4 - on_loan" +
                                "\n\n!!!Введіть назву стовпця. Якщо стовпців декілька - вводити через кому!!!");
                            Console.Write("\nSQLClient > ");
                            columnName = Console.ReadLine();
                            if (columnName == null) Console.WriteLine(inputError);
                            else
                            {
                                Console.Write("Шлях до файлу: ");
                                string file_path = Console.ReadLine(); //Зчитування шляху файла, в який буде записана вибірка
                                if (columnName != null) //Якщо вибрано не вихід з меню запиту вибірки
                                    Console.WriteLine("Запит виконується...");
                                    try { SELECTSpecificQuery(connection, file_path, tables[table], columnName); } //Обробка виключень та виклик SELECT-запиту
                                    catch (Exception exception) { Console.WriteLine(exception.Message); } //Виведення повідомлення помилки
                            }
                        }
                        if (table == 3)
                        {
                            Console.WriteLine("\nСтовпці:\n1 - isbn,\n2 - title_no,\n3 - translation,\n4 - cover,\n5 - loanable" +
                                "\n\n!!!Введіть назву стовпця. Якщо стовпців декілька - вводити через кому!!!");
                            Console.Write("\nSQLClient > ");
                            columnName = Console.ReadLine();
                            if (columnName == null) Console.WriteLine(inputError);
                            else
                            {
                                Console.Write("Шлях до файлу: ");
                                string file_path = Console.ReadLine(); //Зчитування шляху файла, в який буде записана вибірка
                                if (columnName != null) //Якщо вибрано не вихід з меню запиту вибірки
                                    Console.WriteLine("Запит виконується...");
                                try { SELECTSpecificQuery(connection, file_path, tables[table], columnName); } //Обробка виключень та виклик SELECT-запиту
                                catch (Exception exception) { Console.WriteLine(exception.Message); } //Виведення повідомлення помилки
                            }
                        }
                        if (table == 4)
                        {
                            Console.WriteLine("\nСтовпці:\n1 - member_no,\n2 - adult_member_no,\n3 - birth_date" +
                                "\n\n!!!Введіть назву стовпця. Якщо стовпців декілька - вводити через кому!!!");
                            Console.Write("\nSQLClient > ");
                            columnName = Console.ReadLine();
                            if (columnName == null) Console.WriteLine(inputError);
                            else
                            {
                                Console.Write("Шлях до файлу: ");
                                string file_path = Console.ReadLine(); //Зчитування шляху файла, в який буде записана вибірка
                                if (columnName != null) //Якщо вибрано не вихід з меню запиту вибірки
                                    Console.WriteLine("Запит виконується...");
                                try { SELECTSpecificQuery(connection, file_path, tables[table], columnName); } //Обробка виключень та виклик SELECT-запиту
                                catch (Exception exception) { Console.WriteLine(exception.Message); } //Виведення повідомлення помилки
                            }
                        }
                        if (table == 5)
                        {
                            Console.WriteLine("\nСтовпці:\n1 - isbn,\n2 - copy_no,\n3 - title_no,\n4 - member_no,\n5 - out_date,\n6 - due_date" +
                                "\n\n!!!Введіть назву стовпця. Якщо стовпців декілька - вводити через кому!!!");
                            Console.Write("\nSQLClient > ");
                            columnName = Console.ReadLine();
                            if (columnName == null) Console.WriteLine(inputError);
                            else
                            {
                                Console.Write("Шлях до файлу: ");
                                string file_path = Console.ReadLine(); //Зчитування шляху файла, в який буде записана вибірка
                                if (columnName != null) //Якщо вибрано не вихід з меню запиту вибірки
                                    Console.WriteLine("Запит виконується...");
                                try { SELECTSpecificQuery(connection, file_path, tables[table], columnName); } //Обробка виключень та виклик SELECT-запиту
                                catch (Exception exception) { Console.WriteLine(exception.Message); } //Виведення повідомлення помилки
                            }
                        }
                        if (table == 6)
                        {
                            Console.WriteLine("\nСтовпці:\n1 - isbn,\n2 - copy_no,\n3 - out_date,\n4 - member_no,\n5 - due_date,\n6 - in_date,\n7 - fine_assessed,\n8 - fine_paid," +
                                "\n9 - fine_waived,\n10 - remarks" +
                                "\n\n!!!Введіть назву стовпця. Якщо стовпців декілька - вводити через кому!!!");
                            Console.Write("\nSQLClient > ");
                            columnName = Console.ReadLine();
                            if (columnName == null) Console.WriteLine(inputError);
                            else
                            {
                                Console.Write("Шлях до файлу: ");
                                string file_path = Console.ReadLine(); //Зчитування шляху файла, в який буде записана вибірка
                                if (columnName != null) //Якщо вибрано не вихід з меню запиту вибірки
                                    Console.WriteLine("Запит виконується...");
                                try { SELECTSpecificQuery(connection, file_path, tables[table], columnName); } //Обробка виключень та виклик SELECT-запиту
                                catch (Exception exception) { Console.WriteLine(exception.Message); } //Виведення повідомлення помилки
                            }
                        }
                        if (table == 7)
                        {
                            Console.WriteLine("\nСтовпці:\n1 - member_no,\n2 - lastname,\n3 - firstname,\n4 - middleinitial,\n5 - photograph" +
                                "\n\n!!!Введіть назву стовпця. Якщо стовпців декілька - вводити через кому!!!");
                            Console.Write("\nSQLClient > ");
                            columnName = Console.ReadLine();
                            if (columnName == null) Console.WriteLine(inputError);
                            else
                            {
                                Console.Write("Шлях до файлу: ");
                                string file_path = Console.ReadLine(); //Зчитування шляху файла, в який буде записана вибірка
                                if (columnName != null) //Якщо вибрано не вихід з меню запиту вибірки
                                    Console.WriteLine("Запит виконується...");
                                try { SELECTSpecificQuery(connection, file_path, tables[table], columnName); } //Обробка виключень та виклик SELECT-запиту
                                catch (Exception exception) { Console.WriteLine(exception.Message); } //Виведення повідомлення помилки
                            }
                        }
                        if (table == 8)
                        {
                            Console.WriteLine("\nСтовпці:\n1 - isbn,\n2 - member_no,\n3 - log_date,\n4 - remarks" +
                                "\n\n!!!Введіть назву стовпця. Якщо стовпців декілька - вводити через кому!!!");
                            Console.Write("\nSQLClient > ");
                            columnName = Console.ReadLine();
                            if (columnName == null) Console.WriteLine(inputError);
                            else
                            {
                                Console.Write("Шлях до файлу: ");
                                string file_path = Console.ReadLine(); //Зчитування шляху файла, в який буде записана вибірка
                                if (columnName != null) //Якщо вибрано не вихід з меню запиту вибірки
                                    Console.WriteLine("Запит виконується...");
                                try { SELECTSpecificQuery(connection, file_path, tables[table], columnName); } //Обробка виключень та виклик SELECT-запиту
                                catch (Exception exception) { Console.WriteLine(exception.Message); } //Виведення повідомлення помилки
                            }
                        }
                        if (table == 9)
                        {
                            Console.WriteLine("\nСтовпці:\n1 - title_no,\n2 - title,\n3 - author,\n4 - synopsis" +
                                "\n\n!!!Введіть назву стовпця. Якщо стовпців декілька - вводити через кому!!!");
                            Console.Write("\nSQLClient > ");
                            columnName = Console.ReadLine();
                            if (columnName == null) Console.WriteLine(inputError);
                            else
                            {
                                Console.Write("Шлях до файлу: ");
                                string file_path = Console.ReadLine(); //Зчитування шляху файла, в який буде записана вибірка
                                if (columnName != null) //Якщо вибрано не вихід з меню запиту вибірки
                                    Console.WriteLine("Запит виконується...");
                                try { SELECTSpecificQuery(connection, file_path, tables[table], columnName); } //Обробка виключень та виклик SELECT-запиту
                                catch (Exception exception) { Console.WriteLine(exception.Message); } //Виведення повідомлення помилки
                            }
                        }
                        continue;
                    }
                    if (menu == 2) //Меню запитів вибірки
                    {
                        Console.WriteLine("Таблиці:\n0 - вихід,\n1 - adult,\n2 - copy,\n3 - item,\n4 - juvenile,\n5 - loan," +
                            "\n6 - loanhist,\n7 - member,\n8 - reservation,\n9 - title.");
                        Console.Write("SQLClient > ");
                        if (uint.TryParse(Console.ReadLine(), out table) == false || table > 9) Console.WriteLine(inputError); //Перевірка правильності номера таблиці
                        else
                        {
                            Console.Write("Шлях до файлу: ");
                            string file_path = Console.ReadLine(); //Зчитування шляху файла, в який буде записана вибірка
                            if (table != 0) //Якщо вибрано не вихід з меню запиту вибірки
                                try { SELECTQuery(connection, file_path, tables[table]); } //Обробка виключень та виклик SELECT-запиту
                                catch (Exception exception) { Console.WriteLine(exception.Message); } //Виведення повідомлення помилки
                        }
                        continue; //Перейти на наступну ітерацію
                    }
                    if (menu == 3) //Меню запиту на додавання даних в таблицю
                    {
                        Console.WriteLine("Таблиці:\n0 - вихід,\n1 - adult,\n2 - copy,\n3 - item,\n4 - juvenile,\n5 - loan,\n6 - loanhist,\n7 - member,\n8 - reservation,\n9 - title.");
                        Console.Write("SQLClient > ");
                        if (uint.TryParse(Console.ReadLine(), out table) == false || table > 9) Console.WriteLine(inputError); //Перевірка правильності введення
                        else
                            if (table != 0) //Якщо обрано не вихід з меню запиту
                            try { INSERTQuery(connection, tables[table]); } //Обробка виключень та виклик INSERT-запиту
                            catch (Exception ex) { Console.WriteLine(ex.Message); } //Виведення повідомлення помилки
                        continue; //Перейти на наступну ітерацію
                    }
                    if (menu == 4) //Меню запиту на видалення даних з таблиці
                    {
                        Console.WriteLine("Таблиці:\n0 - вихід,\n1 - adult,\n2 - copy,\n3 - item,\n4 - juvenile,\n5 - loan," +
                            "\n6 - loanhist,\n7 - member,\n8 - reservation,\n9 - title.");
                        Console.Write("SQLClient > ");
                        if (uint.TryParse(Console.ReadLine(), out table) == false || table > 9) Console.WriteLine(inputError); //Перевірка правильності введення
                        else
                            if (table != 0) //Якщо обрано не вихід з меню запиту
                            try { DELETEQuery(connection, tables[table]); } //Обробка виключень та виклик DELETE-запиту
                            catch (Exception ex) { Console.WriteLine(ex.Message); } //Виведення повідомлення помилки
                        continue; //Перейти на наступну ітерацію
                    }
                    if (menu == 5) //Меню запиту на оновлення даних таблиці
                    {
                        Console.WriteLine("Таблиці:\n0 - вихід,\n1 - adult,\n2 - copy,\n3 - item,\n4 - juvenile,\n5 - loan,\n6 - loanhist,\n7 - member,\n8 - reservation,\n9 - title.");
                        Console.Write("SQLClient > ");
                        if (uint.TryParse(Console.ReadLine(), out table) == false || table > 9) Console.WriteLine(inputError); //Перевірка правильності введення
                        else
                            if (table != 0) //Якщо обрано не вихід з меню запиту
                            try { UPDATEQuery(connection, tables[table]); } //Обробка виключень та виклик UPDATE-запиту
                            catch (Exception ex) { Console.WriteLine(ex.Message); } //Виведення повідомлення помилки
                        continue; //Перейти на наступну ітерацію
                    }
                }
            }
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Title = "Гуменюк В.Р. Lb8 №8";
            Console.ForegroundColor = ConsoleColor.Green;
            Menu();
            Console.ResetColor();
        }

        static string inputError = "Помилка введення. Спробуйте ще раз."; //Текст помилки
        static Dictionary<uint, string> tables = new Dictionary<uint, string>() //Статичний словник відповідностей число-назва таблиці
        {{ 1, "adult" }, { 2, "copy" }, { 3, "item" }, { 4, "juvenile" }, { 5, "loan" }, { 6, "loanhist" }, { 7, "member" }, { 8, "reservation" }, { 9, "title" }};
    }
}